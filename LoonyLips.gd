extends Node2D




var player_words = [] 

var current_story

func _ready():

	set_random_story()
	$Blackboard/StoryText.text = ""
	check_player_word_length()


func set_random_story():
	var stories = get_from_json("stories.json")
	randomize()
	current_story = stories[randi() % stories.size()]

func get_from_json(filename):
	var file = File.new()
	file.open(filename, File.READ) #TODO: Check if the file exist
	var text = file.get_as_text()
	var data = parse_json(text)
	file.close()
	return data

func _on_TextureButton_pressed():
	if is_story_done() == false:
		var new_text = $Blackboard/TextBox.get_text()
		_on_TextBox_text_entered(new_text)
	else:
		get_tree().reload_current_scene()

func _on_TextBox_text_entered(new_text):
	player_words.append(new_text)
	$Blackboard/TextBox.text = ""
	check_player_word_length()


func check_player_word_length():
	if is_story_done():
		tell_story()
	else:
		prompt_player()

func prompt_player():
	$Blackboard/StoryText.text = ("Can I have " + current_story.prompt[player_words.size()] + ", please?")


func tell_story():
	$Blackboard/StoryText.text = current_story.story % player_words
	$Blackboard/TextureButton/ButtonLabel.text = "Restart!"
	end_game()

func end_game():
	$Blackboard/TextBox.queue_free()
	
	
func is_story_done():
	return player_words.size() == current_story.prompt.size()